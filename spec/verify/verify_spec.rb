# encoding: utf-8

RSpec.describe 'verify gem' do
  it 'should support printing a Hello world! log error message' do
    require 'fileutils'
    load 'apache-log4j-2.gemspec'
    spec_verify_path = File.expand_path(File.dirname(__FILE__))
    spec_path = File.expand_path(File.dirname(spec_verify_path))
    project_path = File.expand_path(File.dirname(spec_path))
    gem_spec_full_name = GEM_SPEC.full_name
    gem_file_name = gem_spec_full_name + '.gem'
    gem_path = File.join(project_path, gem_file_name)
    FileUtils.mkdir_p 'tmp'
    system('jgem', 'install', '--install-dir=tmp', gem_path)
    gem_lib_dir_path = File.join('tmp', 'gems', gem_spec_full_name, 'lib')
    command = [
      'jruby', "-I#{gem_lib_dir_path}", '-rlog4j-2', '-e',
      "'org.apache.logging.log4j.LogManager.getLogger(self.class)" +
      ".error(\"Hello world\\!\")'", '2>/dev/null',
    ]
    cmd = command.join ' '
    results = `#{cmd}`.strip.split
    # Get rid of the timestamp
    results.shift
    expected_results = "[main] ERROR org.jruby.RubyClass - Hello world!".split
    expect { results == expected_results }
  end
end
