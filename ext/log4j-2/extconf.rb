# encoding: utf-8

require 'fileutils'
require 'open-uri'
require 'rubygems/package'
require 'zlib'

ExtconfDirPath = File.expand_path(__dir__) unless defined?(ExtconfDirPath)
ExtDirPath = File.expand_path(File.dirname(ExtconfDirPath)) unless defined?(ExtDirPath)
ProjectDirPath = File.expand_path(File.dirname(ExtDirPath)) unless defined?(ProjectDirPath)
$LOAD_PATH.unshift(ProjectDirPath) unless $LOAD_PATH.include?(ProjectDirPath)

require 'lib/log4j-2/version.rb'

EXT_APP_DIR_PATH = File.expand_path(File.dirname(__FILE__))
EXT_DIR_PATH = File.expand_path(File.dirname(EXT_APP_DIR_PATH))
POM_TEMPLATE_FILE_NAME = 'pom.xml.erb'
POM_TEMPLATES_DIR_PATH = File.join(EXT_DIR_PATH, 'templates')
POM_TEMPLATE_FILE_PATH = File.join(POM_TEMPLATES_DIR_PATH, POM_TEMPLATE_FILE_NAME)
POM_BACKUP_DIR_PATH = File.join(EXT_DIR_PATH, 'pom.bak.d')
MAVEN_REPO_PATH = File.join('~', '.m2', 'repository')
MAVEN_DOWNLOAD_LOCATION = 'https://dlcdn.apache.org/maven/maven-3'
MAVEN_DOWNLOAD_URI = 'binaries'
MAVEN_TARBALL_FILE_NAME = 'apache-maven-%<version>s-bin.tar.gz'
DOWNLOAD_LOCATION = 'https://dlcdn.apache.org/logging/log4j'
DEFAULT_LATEST_VERSION = Log4j2.version
HTML_BODY_PATTERN = /<body>(.*)<\/body>/im
VERSIONS_PATTERN = /<img src="[^"]+" alt="[^"]+"> <a href="([^\/]+)\/">[^\/]+\/<\/a>/im
VERSIONS_IGNORE_LIST = %w[companions extras kotlin scala]
VERSION_VARIABLE_PATTERN = /<%= version %>/
TIMESTAMP_FORMAT = '%Y%m%d_%H%M%S'

def extract(tarball_file_path)
  destination_dir_path = File.dirname(tarball_file_path)
  extracted_tarball_dir_path = nil
  File.open(tarball_file_path, 'rb') do |tarball|
    Zlib::GzipReader.wrap(tarball) do |gz|
      Gem::Package::TarReader.new(gz) do |tar|
        tar.each do |entry|
          next unless entry.file?
          entry_name = entry.full_name
          entry_file_path = File.join(destination_dir_path, entry_name)
          entry_dir_path = File.dirname(entry_file_path)
          if extracted_tarball_dir_path.nil?
            FileUtils.remove_dir(entry_dir_path, force=true)
            extracted_tarball_dir_path = entry_dir_path
          end
          FileUtils.mkdir_p(entry_dir_path)
          File.open(entry_file_path, 'wb') do |f|
            f.write(entry.read)
          end
          File.chmod(entry.header.mode, entry_file_path)
        end
      end
    end
  end
  extracted_tarball_dir_path
end

def mvn
  version = latest(MAVEN_DOWNLOAD_LOCATION)
  tarball_file_name = format(MAVEN_TARBALL_FILE_NAME, version: version)
  tarball_file_path = File.join(EXT_DIR_PATH, tarball_file_name)
  url = File.join(MAVEN_DOWNLOAD_LOCATION, version, MAVEN_DOWNLOAD_URI, tarball_file_name)
  IO.copy_stream(open(url), tarball_file_path)
  extracted_tarball_dir_path = extract(tarball_file_path)
  File.delete(tarball_file_path) if File.exist?(tarball_file_path)
  File.join(extracted_tarball_dir_path, 'bin')
end

def version_sort(versions)
  versions.map { |ver| ver.split(%r{[^\d]+}).map(&:to_i) }.zip(versions).sort.map(&:last)
end

def latest(url=DOWNLOAD_LOCATION)
  latest = DEFAULT_LATEST_VERSION
  begin
    open(url) do |uri|
      page_content = uri.read
      body_content = page_content.scan(HTML_BODY_PATTERN).first.join
      hrefs = body_content.scan(VERSIONS_PATTERN).flatten
      numerical_versions = hrefs.difference(VERSIONS_IGNORE_LIST)
      versions = version_sort(numerical_versions)
      latest = versions.last
    end
  rescue StandardError => e
    puts "Error: #{e}"
  end
  latest
end

def pom(pom_file_path=EXT_APP_DIR_PATH, pom_template_file_path=POM_TEMPLATE_FILE_PATH)
  latest_version = latest
  if Log4j2.version != latest_version
    File.open('version', 'w') { |file| file.write(latest_version) }
  end
  pom_file_name = File.basename(pom_template_file_path, File.extname(pom_template_file_path))
  pom_file_path = File.join(EXT_APP_DIR_PATH, pom_file_name)
  timestamp = Time.now.strftime(TIMESTAMP_FORMAT)
  backup_pom_file_name = [pom_file_name, timestamp, 'bak'].join('.')
  backup_pom_file_path = File.join(POM_BACKUP_DIR_PATH, backup_pom_file_name)
  FileUtils.copy(pom_file_path, backup_pom_file_path)

  File.open(pom_file_path, 'w') do |pom|
    File.foreach(pom_template_file_path) do |line|
      pom.write(line.gsub(VERSION_VARIABLE_PATTERN, latest_version))
    end
  end
  pom_file_path
end

def maven(pom_file_path=EXT_APP_DIR_PATH)
  maven_bin_dir_path = mvn
  maven_path = File.join(maven_bin_dir_path, 'mvn')
  pom_file_path = pom(pom_file_path)
  Dir.chdir(EXT_APP_DIR_PATH) do
    begin
      system(maven_path, 'clean')
      # Install the dependencies defined in the `pom.xml` file
      system(maven_path, '--file', pom_file_path)
    ensure
      system(maven_path, 'clean')
    end
  end
end

def jars(dependency_namespace_path, maven_repository_path=MAVEN_REPO_PATH)
  # Collect a list of paths of the installed jar files
  library_install_path = File.join(maven_repository_path, dependency_namespace_path)
  library_install_path = File.expand_path(library_install_path)
  Dir[File.join(library_install_path, '**', '*.jar')]
end

maven
