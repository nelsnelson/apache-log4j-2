# -*- mode: ruby -*-
# vi: set ft=ruby :
# encoding: utf-8

require_relative 'lib/log4j-2/version.rb'

GEM_SPEC = Gem::Specification.new do |spec|
  spec.name        = 'apache-log4j-2'
  spec.version     = Log4j2.release_version
  spec.date        = Time.now.strftime('%Y-%m-%d')
  spec.summary     = "Apache Log4j 2 java jars packaged as a gem."
  spec.description = "Apache Log4j 2 is an upgrade to Log4j that provides " +
    "significant improvements over its predecessor, Log4j 1.x, and provides " +
    "many of the improvements available in Logback while fixing some " +
    "inherent problems in Logback's architecture."
  spec.authors     = ['Nels Nelson']
  spec.email       = 'nels@nelsnelson.org'
  spec.files       = [
    Dir['*.gemspec'], Dir[File.join('lib', '**', '*.rb')],
    Dir[File.join('lib', '**', '*.jar')],
    'LICENSE.txt', 'Rakefile', 'README.md',
  ].flatten
  spec.extensions  = Dir[File.join('ext', '**', 'extconf.rb')]
  spec.platform    = 'java'
  spec.homepage    = 'https://rubygems.org/gems/apache-log4j-2'
  spec.metadata    = {
    'source_code_uri': 'https://gitlab.com/nelsnelson/apache-log4j-2',
    'bug_tracker_uri': 'https://gitlab.com/nelsnelson/apache-log4j-2/issues',
    'canonical_pom_uri': 'https://search.maven.org/remotecontent' +
      '?filepath=org/apache/logging/log4j/log4j/2.12.1/log4j-2.12.1.pom'
  }
  spec.license     = 'Apache-2.0'

  spec.required_ruby_version = '>= 2.5.3'
  spec.add_development_dependency 'rake', '~> 12.3', '>= 12.3.2'
  spec.add_development_dependency 'rspec', '~> 3.8', '>= 3.8.0'
end

