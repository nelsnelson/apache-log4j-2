# apache-log4j-2

This packages the Apache Log4j 2 jars into a convenient gem for requiring.

```rb
require 'log4j-2'
```

## Setup

To setup the development environmet for building the gem, execute:

```sh
asdf plugin add ruby
asdf list all ruby > /dev/null
asdf install ruby
gem install bundler
bundle install
```

## Building

To clean the project, run unit tests, build the gem file, and verify that the built artifact works, execute:

```sh
bundle exec rake clobber package spec gem verify
```


## Publish

To publish the gem, execute:

```sh
bundle exec rake publish
```

