# encoding: utf-8

module Log4j2
  def self.version
    File.read('version')
  end

  def self.release
    File.read('release')
  end

  def self.release_version(release=Log4j2.release)
    release_version = [Log4j2.version]
    release_version << release unless release.nil?
    release_version.join('.').freeze
  end

  VERSION = Log4j2.version
  GEM_RELEASE = Log4j2.release
end
